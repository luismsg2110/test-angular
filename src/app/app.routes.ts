import { RouterModule, Routes } from '@angular/router';


import { LoginComponent } from './components/login/login.component';
import { ListComponent } from './components/list/list.component';
import { SearchComponent } from './components/search/search.component';

const app_routes: Routes = [
    {path: 'login', component: LoginComponent},
    {path: 'list', component: ListComponent},
    {path: 'search/:param', component: SearchComponent},
    {path: '', pathMatch: 'full', redirectTo: 'login'}
];

export const app_routing = RouterModule.forRoot(app_routes, {useHash:true});
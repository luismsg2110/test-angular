import { FormGroup, Validators, FormBuilder, ReactiveFormsModule} from '@angular/forms'
import { Component, OnInit } from '@angular/core';
import { RestloginService } from '../../services/restlogin.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  formLogin:FormGroup;
  info:any[] = [];
  errorMsg:any;
  errores:any[]=[];
  correo:string;

  constructor( private fb:FormBuilder, private _rs:RestloginService,  private router:Router) { 
    this.formLogin = this.fb.group({
      email : ['', Validators.email],
      password : ['', Validators.required]
    });

  }

  ngOnInit() {
  }

  login (){  
    this.correo = this.formLogin.get('email').value;  
    //console.log(this._rs.doLogin(<string>this.correo));
    this._rs.doLogin(<string>this.correo).then( ()=>{
    console.log(this._rs.errorBool);
    if (this._rs.errorBool){

    }else{
      this.router.navigate(['list']);
    }
    });

  }

}

import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';
import { reject } from 'q';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json', 'app':'APP_BCK',  'password':'1234' })
};

@Injectable()
export class RestloginService {
  public url:string;
  info:any;
  errorMsg:any;
  errores:any[]=[];
  errorBool:boolean = false;
  body:any = {};

  constructor( public http: HttpClient ) {
    //this.url = "https://dev.tuten.cl:443/TutenREST/rest/user/";
   }
  
  doLogin(email:string){
    //this.http.get(`https://dev.tuten.cl:443/TutenREST/rest/user/${ email }`, httpOptions);
    let promise = new Promise ((resolve, reject) =>{
      this.http.put(`https://dev.tuten.cl:443/TutenREST/rest/user/${ email }`, this.body, httpOptions)
      .subscribe( data =>{
        this.info = data;
        this.errorBool = false;
        resolve();
        
      },
      err => {
        this.errores = err;
        this.errorMsg = err.error;
        this.errorBool = true;
        resolve();
      });
    });
    return promise;
  }



}

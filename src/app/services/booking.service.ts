import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';
import { reject } from 'q';

@Injectable()
export class BookingService {
  book:any; 
  errorBool:boolean = false;
  head:any = {};

  constructor(public http: HttpClient) {
  
   }

   showBook(adminemail:string, token:string){
    this.head = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json', 'app':'APP_BCK',  'adminemail':`${adminemail}`, 'token':`${token}` })
    };
    let promise = new Promise ((resolve, reject) =>{
      this.http.get(`https://dev.tuten.cl:443/TutenREST/rest/user/miguel@tuten.cl/bookings?current=true`, this.head)
      .subscribe( data =>{
        this.book = data;
        this.errorBool = false;
        resolve();
      },
      err => {
        this.errorBool = true;
        resolve();
      });
    });
    return promise;
  }
}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormGroup, Validators, FormBuilder, FormsModule, ReactiveFormsModule} from '@angular/forms'
import { AgGridModule } from 'ag-grid-angular/main';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { LoginComponent } from './components/login/login.component';
import { ListComponent } from './components/list/list.component';

import {FilterPipe} from './components/list/pipe.custom';

import { SearchComponent } from './components/search/search.component';
import {HttpClientModule} from '@angular/common/http';
import { RestloginService } from './services/restlogin.service';
import { BookingService } from './services/booking.service';


//Rutas
import { app_routing } from './app.routes';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    ListComponent,
    SearchComponent, 
    FilterPipe
  ],
  imports: [
    BrowserModule,
    app_routing,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AgGridModule.withComponents([])
    
  ],
  providers: [
    RestloginService,
    BookingService
  ],
  exports: [
    FilterPipe
],
  bootstrap: [AppComponent]
})
export class AppModule { }

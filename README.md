# Desarrollo

Para realizar esta aplicación, si emplearon los componentes:

-- @angular/http": "^5.2.0" ==>> para hacer las peticiones HTTP a través de promesas como se ve a continuación:

```
let promise = new Promise ((resolve, reject) =>{
      this.http.put(`https://dev.tuten.cl:443/TutenREST/rest/user/${ email }`, this.body, httpOptions)
      .subscribe( data =>{
        this.info = data;
        this.errorBool = false;
        resolve();
      },
      err => {
        this.errores = err;
        this.errorMsg = err.error;
        this.errorBool = true;
        resolve();
      });
    });
    return promise;
  }
```
  
Usando la bandera `errorBoll` pra saber si la peticion retorna una respuesta correcta o erronea. 
En caso de ser correcta, la variable `info` almacena la respuesta de la api.

-- "@angular/forms": "^5.2.0" 

Para validar y capturar los valores del formulario (front html) para insertarlos en la petición.
Ejemplo de validación:

```
this.formLogin = this.fb.group({
      email : ['', Validators.email],
      password : ['', Validators.required]
    });
```
	

-- Se crearon los servicios RestloginService (para realizar el login) y BookingService (para listar los booking del usuario).

--Para el filtrado del bookingId se realizó un filtrado personalizado, como se muestra a continuación:

```
@Pipe({
  name : 'filter',
})
export class FilterPipe implements PipeTransform {
  public transform(value, key: string, term: string) {
    return value.filter((item) => {
      if (item.hasOwnProperty(key)) {
        if (term) {
          let regExp = new RegExp('\\b' + term, 'gi');
          return regExp.test(item[key]);
        } else {
          return true;
        }
      } else {
        return false;
      }
    });
  }
}
```

-------------------------------------------------------------------------------------------------------------------------------

# LoginApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.5.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
